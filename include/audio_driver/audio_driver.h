#pragma once

#include <ros/ros.h>
#include <string>

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include "bass/bass.h"

// sleep function, copied from linux/windows compat code
// I kept the SLEEP def because i liked giving millis
#include <unistd.h>
#define SLEEP( milliseconds ) usleep( (unsigned long) (milliseconds * 1000.0) )


#define BUFSTEP 200000	// memory allocation unit

static bool BASS_INIT_COMPLETE = false;

class AudioDriver {
public:
    AudioDriver(int input, int sample_rate, int channels);
    ~AudioDriver();

    // Buffer Handling
    DWORD getBuffer(char* & buffer);
    char* getWavHeader();
    void flushBuffer();

    //control functions
    void startStream();
    void stopStream();
    void WriteFile(std::string file_name);

    bool isError();
    int getState();
    std::string getStateString();

    BOOL CALLBACK BASS_Callback(HRECORD handle, const void *buffer, DWORD length);

private:
    //operating variables
    int input_source_;
    int sample_rate_;
    int channels_;
    int state_;
    bool error_;

    boost::mutex buffer_mutex_;

    //buffer variables
    char* buffer_ = 0;
    DWORD recieved_length_ = 0;
    HRECORD channel_ =0;

};

//Enumerated AudioDriver states
enum AD_States {
    ADS_NO_ERROR = 0,
    ADS_ERROR = 1,
    ADS_IDLE = 0,
    ADS_RECORDING = 1,
    ADS_WRONG_VERSION = 2,
    ADS_DEVICE_ERROR = 3,
    ADS_ALLOC_ERROR = 4,
    ADS_START_ERROR = 5
};

//strings represeting audio driver state
static std::string ADS_Strings[] = {
    "Idle",
    "Recording to buffer",
    "An incorrect version of BASS was loaded",
    "Can't initialize recording device",
    "Allocation Error.",
    "Unknown Start Error."
};
