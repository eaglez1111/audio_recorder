#pragma once

#include <iostream>
#include <ros/ros.h>
#include <std_msgs/Empty.h>
#include <robot_config/AudioConfig.h>

#include "audio_driver/audio_driver.h"

class AudioRecorder {
public:
    AudioRecorder(ros::NodeHandle nh, std::string buffer_topic, int sample_rate,
        int channels, int input_source, bool follow_commands = true,
        std::string start_recording_topic = "/audio/start", std::string stop_recording_topic = "/audio/stop",
        bool publish_configuration = "true", std::string configuration_topic = "/audio/config");

    void start_callback(const std_msgs::Empty::ConstPtr& data);
    void stop_callback(const std_msgs::Empty::ConstPtr& data);

    void handle_audio();

private:
    void publish_buffer();
    void publish_configuration();

private:
    //! the ros nodehandle
    ros::NodeHandle nh_;
    //! the publisher used to publish the buffer
    ros::Publisher buffer_publisher_;

    bool publish_configuration_;
    ros::Publisher configuration_pub_;
    robot_config::AudioConfig config_msg_;

    // Control objects
    ros::Subscriber start_sub_;
    ros::Subscriber stop_sub_;
    bool recording_ = false;

    //! AudioDriver object to access audio buffer easily
    AudioDriver* audio_driver_;

    //! the buffer to publish
    char* current_buffer_ = 0;
    long int buffer_length_ = 0;
};
