#include <string.h>
#include <malloc.h>
#include <regex.h>
#include <stdio.h>
#include <signal.h>
#include "audio_driver/audio_driver.h"

/**
* @brief CallBackShim() handles callbacks for the AudioDriver class
* @param [in] handle passed to the Callback function in the class
* @param [in] buffer passed to the Callback function in the class
* @param [in] length passed to the Callback function in the class
* @param [in] user pointer to the class on which to call the callback
* @return BOOL true/false whether to continue recording
*/
BOOL CALLBACK CallBackShim(HRECORD handle, const void *buffer, DWORD length, void *user) {
    return ((AudioDriver*)user)->BASS_Callback(handle, buffer, length);
}

/**
* @brief release_memory() releases memory safely when a shutdown signal is recieved
* @param [in] sig signal from shutdown
*/
void release_memory(int sig) {
    (void) sig;

    BASS_RecordFree();
    BASS_Free();
    std::cout << "Audio driver released." << std::endl;
}

/**
* @brief AudioDriver() initializes the AudioDriver class
* @param [in] input source to enable to record audio from
* @param [in] sample_rate rate to sample audio at in Hz
* @param [in] channels channels of audio to record
* @details Initializes AudioDriver object, sets up kill signal for releasing
* memory. Sets default state. Initializes Audio Drivers, checks for errors.
* Enables the correct audio input to record.
*/
AudioDriver::AudioDriver(int input, int sample_rate, int channels):
  input_source_(input), sample_rate_(sample_rate), channels_(channels) {

    signal(SIGINT, release_memory);

    state_ = ADS_IDLE;
    error_ = ADS_NO_ERROR;

    if (HIWORD(BASS_GetVersion())!=BASSVERSION) {
		printf("An incorrect version of BASS was loaded\n");
        state_ = ADS_WRONG_VERSION;
        error_ = ADS_ERROR;
        return;
	}

	// initialize default recording device
	if (!BASS_INIT_COMPLETE && !BASS_RecordInit(-1)) {
		printf("Can't initialize recording device\n");
        state_ = ADS_DEVICE_ERROR;
        error_ = ADS_ERROR;
		return;
	} else {
        BASS_INIT_COMPLETE = true;
    }

    int i;
    //disable all inputs
    for (i=0;BASS_RecordSetInput(i,BASS_INPUT_OFF,-1);i++);
    //enable the selected
    BASS_RecordSetInput(input,BASS_INPUT_ON,-1);
}

/**
* @brief ~AudioDriver() destructs object by releasing memory.
*/
AudioDriver::~AudioDriver() {
    BASS_RecordFree();
	BASS_Free();
}

/**
* @brief startStream() starts recording from source into the buffer
*/
void AudioDriver::startStream() {
    WAVEFORMATEX *wf;
    if (buffer_) { // free old recording
        BASS_StreamFree(channel_);
        channel_=0;
        free(buffer_);
        buffer_=NULL;
        BASS_Free();
    }
    // allocate initial buffer and make space for WAVE header
    buffer_=(char*)malloc(BUFSTEP);
    recieved_length_ = 0;

    if (!(channel_=BASS_RecordStart(sample_rate_,channels_,0, &CallBackShim,this))) {
        printf("Couldn't start recording\n");
        error_ = ADS_ERROR;
        state_ = ADS_START_ERROR;
        free(buffer_);
        buffer_=0;
        return;
    }
}

/**
* @brief stopStream() stops the driver from recording to the buffer
*/
void AudioDriver::stopStream() {
    BASS_ChannelStop(channel_);
    channel_ = 0;

    // enable "save" button
    // setup output device (using default device)
    if (!BASS_Init(-1,44100,0,NULL,NULL)) {
        printf("Can't initialize output device\n");
        error_ = ADS_ERROR;
        state_ = ADS_DEVICE_ERROR;
        return;
    }
}

/**
* @brief getBuffer() transfers the recorded buffer to a memory location
* @param [in] buffer location of memory to copy buffer to
* @return length of buffer
* @details copyies buffer to memory location, flushes buffer
*/
DWORD AudioDriver::getBuffer(char* & buffer) {
    boost::mutex::scoped_lock lock(buffer_mutex_);
    buffer=(char*)malloc(recieved_length_);
    memcpy(buffer, buffer_, recieved_length_);
    DWORD return_value = recieved_length_;

    flushBuffer();

    return return_value;
}

/**
* @brief flushBuffer() clears the internal buffer
* @details reallocates internal buffer, sets buffer length to 0
*/
void AudioDriver::flushBuffer() {
    //reallocate buffer
    buffer_=(char*)realloc(buffer_, BUFSTEP);
    recieved_length_ = 0;
}

/**
* @brief isError() returns whether the driver is in error state or not
* @return true if in error state, false if not
*/
bool AudioDriver::isError() {
    return error_;
}

/**
* @brief getState() returns the current Driver state
* @return int representing recorder state
*/
int AudioDriver::getState() {
    return state_;
}

/**
* @brief getStateString() returns a string value of the current state
* @return string representing current state
*/
std::string AudioDriver::getStateString() {
    return ADS_Strings[state_];
}

/**
* @brief BASS_Callback() records to the buffer from the audio driver
* @param [in] handle allows the AudioDriver class to access the recorder
* @param [in] buffer is the memory location of the buffer from the low level driver
* @param [in] length is the length of the buffer to be copied from
* @return BOOL true if buffer allocated safely, false if not
*/
BOOL CALLBACK AudioDriver::BASS_Callback(HRECORD handle, const void *buffer, DWORD length) {
	// increase buffer size if needed
    boost::mutex::scoped_lock lock(buffer_mutex_);
	if ((recieved_length_%BUFSTEP)+length>=BUFSTEP) {
		buffer_=(char *)realloc(buffer_,((recieved_length_+length)/BUFSTEP+1)*BUFSTEP);
		if (!buffer_) {
			channel_=0;
		    error_ = ADS_ERROR;
            state_ = ADS_ALLOC_ERROR;
			return FALSE; // stop recording
		}
	}
	// buffer the data
	memcpy(buffer_+recieved_length_,buffer,length);
	recieved_length_+=length;
	return TRUE; // continue recording
}
