#include "audio_recorder/audio_recorder.h"
#include "robot_config/Audio.h"

/**
* @brief AudioRecorder() initialzes an AudioRecorder node
* @param [in] nh nodehandle to generate subscribers and publishers on
* @param [in] buffer_topic topic on which to publish audio data
* @param [in] sample_rate rate at which to sample audio in Hz
* @param [in] channels channels to record audio at
* @param [in] input_source audio source from which to record
* @param [in] follow_commands whether or not to listen to start/stop commands
* @param [in] start_recording_topic topic to listen to to start recording
* @param [in] stop_recording_topic topic to listen to to stop recording
* @param [in] publish_configuration topic on which to publish configuration
* @param [in] configuration_topic topic to publish configuration to
* @details initalizes audio driver, buffer publisher, configuration publisher
*/
AudioRecorder::AudioRecorder(ros::NodeHandle nh, std::string buffer_topic,
  int sample_rate, int channels, int input_source, bool follow_commands,
  std::string start_recording_topic, std::string stop_recording_topic,
  bool publish_config, std::string configuration_topic):

  nh_(nh), buffer_publisher_(nh.advertise<robot_config::Audio>(buffer_topic, 10)),
  publish_configuration_(publish_config)  {

    //if configured to publish config define the publisher and message
    if(publish_config) {
        configuration_pub_ = nh.advertise<robot_config::AudioConfig>(configuration_topic, 10);
        config_msg_.input_source = input_source;
        config_msg_.sample_rate = sample_rate;
        config_msg_.channels = channels;
    }

    //if configured to follow commands define the command subscribers and listen to them
    if(follow_commands) {
        start_sub_ = nh.subscribe(start_recording_topic, 10, &AudioRecorder::start_callback, this);
        stop_sub_ = nh.subscribe(stop_recording_topic, 10, &AudioRecorder::stop_callback, this);
    } else {
        //else just set record to true and it will continue recording forever
        recording_ = true;
        //since we dont have a command to listen to to publish the configuration
        //to we will just publish it now
        publish_configuration();
    }

    //initialize the audio driver according to the config
    audio_driver_ = new AudioDriver(input_source, sample_rate, channels);
}


void AudioRecorder::start_callback(const std_msgs::Empty::ConstPtr& data) {
    //stop if already recording
    if(recording_)
        return;

    //flush the buffer if any data is already there
    audio_driver_->flushBuffer();
    audio_driver_->startStream();
    //flag node as recording
    recording_ = true;

    //publish the configuration if configured to
    if(publish_configuration_) {
        publish_configuration();
    }
}

/**
* @brief start_callback()
* @param [in] data data from the ros topic
* @return <return_description>
* @details <details>
*/
void AudioRecorder::stop_callback(const std_msgs::Empty::ConstPtr& data) {
    //stop if not recording
    if(!recording_)
        return;

    //stop the stream and flag the node as not recording
    audio_driver_->stopStream();
    recording_ = false;
}

/**
* @brief handle_audio() processes the audio recieved from driver since last call
* @details Calls read_in_buffer() then publishes the data.
* this is made to be flexible to allow audio processing to be added if desired.
*/
void AudioRecorder::handle_audio() {
    //free the current buffer if it exists
    if(current_buffer_) {
        free(current_buffer_);
        current_buffer_ = NULL;
        buffer_length_ = 0;
    }

    //stop here if not recording
    if(!recording_)
        return;

    //check for audio driver errors
    if(!audio_driver_->isError())
        buffer_length_ = audio_driver_->getBuffer(current_buffer_);
    else
        ROS_ERROR("[AUDIO DRIVER ERROR]: %s", audio_driver_->getStateString().c_str());

    //publish the buffer we just got
    publish_buffer();
}

/**
* @brief publish_configuration() publishes the audio config to the publisher
* @details made very simple in it's own function to expand on later if wanted
*/
void AudioRecorder::publish_configuration() {
    configuration_pub_.publish(config_msg_);
}

/**
* @brief publish_buffer() publishes the current buffer
* @details converts the buffer into an audio message then publishes it
*/
void AudioRecorder::publish_buffer() {
    //generate message
    robot_config::Audio message;
    message.header.stamp = ros::Time::now();
    message.data.clear();

    //populate the audio message with data from the buffer
    int i;
    for(i = 0; i < buffer_length_; i ++)
        message.data.push_back(current_buffer_[i]);

    //publish
    buffer_publisher_.publish(message);
}
