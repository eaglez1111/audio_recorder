#include <ros/ros.h>
#include <ostream>

#include "audio_recorder/audio_recorder.h"

int main(int argc, char** argv) {
    // Initialize ROS node
    ros::init(argc, argv, "audio_recorder_node");
    ros::NodeHandle nh("~");

    // get parameters
    std::string audio_topic;
    std::string start_recording_topic;
    std::string stop_recording_topic;
    std::string configuration_topic;

    int input_source;
    int sample_rate;
    int channels;

    bool publish_configuration;
    bool follow_commands;

    nh.param<std::string>("audio_topic",            audio_topic,            "/audio/stream");
    nh.param<std::string>("start_recording_topic",  start_recording_topic,  "/audio/start");
    nh.param<std::string>("stop_recording_topic",   stop_recording_topic,   "/audio/stop");
    nh.param<std::string>("configuration_topic",    configuration_topic,    "/audio/config");

    nh.param<int>("input_source",           input_source,   0);
    nh.param<int>("sample_rate",            sample_rate,    44100);
    nh.param<int>("channels",               channels,       1);

    nh.param<bool>("publish_configuration", publish_configuration,  true);
    nh.param<bool>("follow_commands",       follow_commands,        true);

    // Print configuration
    ROS_INFO("Audio recorder node publishing audio to %s.", audio_topic.c_str());
    ROS_INFO("Audio recorder node recording from audio source %d.", input_source);
    ROS_INFO("Audio recorder node recording at %d Hz over %d channels.", sample_rate, channels);
    ROS_INFO("Audio recorder node listening to '%s' to start recording.", start_recording_topic.c_str());
    ROS_INFO("Audio recorder node listening to '%s' to stop recording.", stop_recording_topic.c_str());
    if(publish_configuration) {
        ROS_INFO("Audio recorder node will publish a configuration to %s.", configuration_topic.c_str());
    }

    // Initialize recorder node
    AudioRecorder recorder_node(nh, audio_topic, sample_rate, channels,
        input_source, follow_commands, start_recording_topic, stop_recording_topic,
        publish_configuration, configuration_topic);

    //loop and handle audio
    ros::Rate loop(1);
    while(ros::ok()) {
        recorder_node.handle_audio();

        ros::spinOnce();
        loop.sleep();
    }

    // Exit
    ROS_INFO("Exiting Audio Recorder.");
    return 0;
}
